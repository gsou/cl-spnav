(in-package :cl-spnav)

(defun load-library ()
  "Load the libspnav.so library"
  (cffi:define-foreign-library libspnav (t "libspnav.so"))
  (cffi:load-foreign-library 'libspnav)
  (cffi:reload-foreign-libraries))

(cffi:defcstruct spnav-event-motion
  (type :int)
  (x :int)
  (y :int)
  (z :int)
  (rx :int)
  (ry :int)
  (rz :int)
  (period :unsigned-int)
  (data (:pointer :int)))

(cffi:defcstruct spnav-event-button
  (type :int)
  (press :int)
  (bnum :int))

(cffi:defcunion spnav-event
  (type :int)
  (motion (:struct spnav-event-motion))
  (button (:struct spnav-event-button)))

(cffi:defctype spnav-event (:union spnav-event))

(cffi:defcstruct spnav-wrapper
  (inner (:union spnav-event)))

(cffi:defcfun ("spnav_open" spnav-open-raw) :int
  "Open connection to the daemon via AF_UNIX socket.
The unix domain socket interface is an alternative to the original magellan
protocol, and it is *NOT* compatible with the 3D connexion driver. If you wish
to remain compatible, use the X11 protocol (spnav_x11_open, see below).
Returns -1 on failure.")

(cffi:defcfun ("spnav_close" spnav-close-raw) :int
  "Close connection to the daemon. Use it for X11 or AF_UNIX connections.
Returns -1 on failure")

(cffi:defcfun ("spnav_fd" spnav-fd) :int
  "Retrieves the file descriptor used for communication with the daemon, for
use with select() by the application, if so required.
If the X11 mode is used, the socket used to communicate with the X server is
returned, so the result of this function is always reliable.
If AF_UNIX mode is used, the fd of the socket is returned or -1 if
no connection is open / failure occured.")

(cffi:defcfun ("spnav_sensitivity" spnav-sensitivity) :int
  (sens :double))

(cffi:defcfun ("spnav_wait_event" spnav-wait-event-raw) :int
  "Blocks waiting for space-nav events. returns 0 if an error occurs */"
  (event (:pointer spnav-event)))

(cffi:defcfun ("spnav_poll_event" spnav-poll-event-raw) :int
  "checks the availability of space-nav events (non-blocking)
returns the event type if available, or 0 otherwise."
  (event (:pointer spnav-event)))

(cffi:defcfun ("spnav_remove_events" spnav-remove-events) :int
  "Removes any pending events from the specified type, or all pending events
events if the type argument is SPNAV_EVENT_ANY. Returns the number of
removed events."
  (type :int))

(defconstant +spnav-event-any+ 0)
(defconstant +spnav-event-motion+ 1)
(defconstant +spnav-event-button+ 2)
