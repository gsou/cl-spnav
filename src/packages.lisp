(defpackage :cl-spnav
  (:use :cl)
  (:nicknames :spnav)
  (:export
   #:load-library
   #:spnav-open
   #:spnav-close
   #:spnav-wait-event
   #:spnav-poll-event
   #:with-spnav
   #:with-spnav-event
   #:with-spnav-rebind
   #:get-enterprise-button-name))
