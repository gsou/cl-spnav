(in-package :cl-spnav)

(defun spnav-open ()
  "Open the connection to the daemon."
  (let ((ret (spnav-open-raw)))
    (when (= ret -1) (error "Could not open connection to the spacenavd daemon"))
    (values)))

(defun spnav-close ()
  "Close the connection to the daemon."
  (let ((ret (spnav-close-raw)))
    (when (= ret -1) (error "Could not close connection to the spacenavd daemon"))
    (values)))

(declaim (optimize (debug 3) (safety 3) (speed 0)))
(defun spnav-wait-event-ptr ()
  "Blocks waiting for a spnav event, returns a pointer to the event"
  (let* ((ptr (cffi:foreign-alloc '(:union spnav-event)))
         (ret (spnav-wait-event-raw ptr)))
    (when (= ret 0) (error "Error while waiting for spnav event"))
    (cffi:mem-ref ptr '(:union spnav-event))))

(defun spnav-poll-event-ptr ()
  "Blocks waiting for a spnav event, returns a pointer to the event"
  (let* ((ptr (cffi:foreign-alloc '(:union spnav-event)))
         (ret (spnav-poll-event-raw ptr)))
    (if (= ret 0)
        (progn
          (cffi:foreign-free ptr) nil)
        (cffi:mem-ref ptr '(:union spnav-event)))))

(defun get-event-from-ptr (ptr)
  "Extract the event from the PTR"
  (cadr (cffi:mem-ref ptr '(:struct spnav-wrapper))))

(defun parse-event (event)
        (let* (
               (type (getf event 'type))
               (button (getf event 'button))
               (bnum (getf button 'bnum))
               (press (getf button 'press)))
          ;; TODO Motion events
          (cond
            ((= type +spnav-event-button+)
             (list :button bnum (/= 0 press)))
            ((= type +spnav-event-motion+) (list :motion))
            (t (error "Invalid event type ~A" type)))))

(defun spnav-wait-event (&key raw)
  "Blocks waiting for a spnav event, if RAW is set, return the raw event instead of the parsed version"
  (let ((ptr (spnav-wait-event-ptr)))
    (prog1
        (let* ((event (get-event-from-ptr ptr)))
          (if raw
              event
              (parse-event event)))
      (cffi:foreign-free ptr))))

(defun spnav-poll-event (&key raw)
  (let ((ptr (spnav-poll-event-ptr)))
    (if ptr
        (prog1
            (let* ((event (get-event-from-ptr ptr)))
              (if raw
                  event
                  (parse-event event)))
          (cffi:foreign-free ptr)))))

(defmacro with-spnav (&body body)
  "Run the BODY wrapped in the required init calls"
  `(unwind-protect
        (progn
          (spnav-open)
          ,@body)
     (spnav-close)))

(defmacro with-spnav-event ((ev) &body body)
  "Execute BODY everytime an event is trigger. The event is bound to EV"
  `(with-spnav
    (loop
      (let ((,ev (spnav-wait-event)))
        ,@body))))

(defun get-enterprise-button-name (num)
  "Get the SpaceMouse button name from the button number"
  (case num
    ;; NOTE Space, Tab, V1, V2, V3, 11 and 12 also returns a number 0
    (0 "MENU")
    (1 "FIT")
    (2 "Top")
    (4 "Right")
    (5 "Front")
    (8 "Rotate")
    (10 "ISO 1")
    (12 "1")
    (13 "2")
    (14 "3")
    (15 "4")
    (16 "5")
    (17 "6")
    (18 "7")
    (19 "8")
    (20 "9")
    (21 "10")
    (22 "ESC")
    (23 "ALT")
    (24 "SHIFT")
    (25 "CTRL")
    (26 "LOCK")
    (35 "ENTER")
    (36 "DELETE")))

(defun get-enterprise-mapping ()
  "Returns an alist that can be used with `with-spnav-rebind' for the SpaceMouse Enterprise"
  `((22 . 9)                            ; Escape
    (23 . 64)                           ; Alt_L
    (24 . 50)                           ; Shift_L
    (25 . 37)                           ; Control_L
    (35 . 36)                           ; Return
    (36 . 119)                          ; Delete
    ))

(defmacro with-default-display ((display &key (force nil)) &body body)
  `(let ((,display (xlib:open-default-display)))
     (unwind-protect
          (unwind-protect
               ,@body
            (when ,force
              (xlib:display-force-output ,display)))
       (xlib:close-display ,display))))

(defun set-key-state (code state)
  (with-default-display (d :force t) (xlib/xtest:fake-key-event d code state)))

(defun with-spnav-rebind (map)
  "MAP is an alist of (bnum . keycode). The buttons of the SPNAV bnum will be rebound to the batching keyboard keycode."
  (with-spnav-event (event)
    (trivia:match event
      ((list :button bnum state)
       (let ((keycode (cdr (assoc bnum map))))
         (if keycode (set-key-state keycode state)))))))
