(asdf:defsystem #:cl-spnav
  :description "Bindings to libspnav"
  :version "0.1"
  :author "gsou"
  :depends-on ("cffi" "clx" "trivia")
  :license "MIT"
  :pathname "src/"
  :serial t
  :components ((:file "packages")
               (:file "bindings")
               (:file "interface")
               ))
